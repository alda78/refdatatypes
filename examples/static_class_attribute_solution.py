from refdatatypes.refint import RefInt


class AAA:
    static = RefInt(1)


class BBB(AAA):
    pass


print(f"refint: {AAA.static.value} addr: {AAA.static.get_ref_addr()}")  # get 1 correctly
print(f"refint: {BBB.static.value} addr: {BBB.static.get_ref_addr()}")  # get 1 correctly

AAA.static.value = 5  # access value via reference defined in class AAA
print(f"refint: {AAA.static.value} addr: {AAA.static.get_ref_addr()}")  # get 5 correctly
print(f"refint: {BBB.static.value} addr: {BBB.static.get_ref_addr()}")  # get 5 correctly

BBB.static.value = 6  # static attribute is not defined in class BBB, parent attribute will be used. Attribute would be changed, only referenced value of it.
print(f"refint: {AAA.static.value} addr: {AAA.static.get_ref_addr()}")  # get 6 correctly
print(f"refint: {BBB.static.value} addr: {BBB.static.get_ref_addr()}")  # get 6 correctly

AAA.static.value = 7
print(f"refint: {AAA.static.value} addr: {AAA.static.get_ref_addr()}")  # get 7 correctly
print(f"refint: {BBB.static.value} addr: {BBB.static.get_ref_addr()}")  # get 7 correctly

# for read operations you can simply access value, there are conversions into basic datatypes implemented
print(f"refint value={AAA.static}")

# if you need to change value, you have to access via `.value` property
AAA.static.value += 5
print(f"refint value={AAA.static}")
