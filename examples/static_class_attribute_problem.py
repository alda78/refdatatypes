class A:
    static = 1000  # disable python number caching for walues up to 255


class B(A):
    pass


print(f"int: {A.static} addr:{id(A.static)}")  # get 1000 correctly
print(f"int: {B.static} addr:{id(B.static)}")  # get 1000 correctly, static attribute is not defined in class B, Python start looking for it in parent class

A.static = 5000
print(f"int: {A.static} addr:{id(A.static)}")  # get 5000 correctly
print(f"int: {B.static} addr:{id(B.static)}")  # get 5000 correctly, static attribute is not defined in class B, Python start looking for it in parent class

B.static = 6000  # static attribute is still not defined in class B, Pytohn decide to created it at new address place as a part of class B
print(f"int: {A.static} addr:{id(A.static)}")  # expected 6000, but get 5000, because class A and B have now defined static attribute separately
print(f"int: {B.static} addr:{id(B.static)}")  # get 6000 correctly

A.static = 7000
print(f"int: {A.static} addr:{id(A.static)}")  # get 7000 correctly
print(f"int: {B.static} addr:{id(B.static)}")  # get unchanged 6000, because class has its own static attribute
