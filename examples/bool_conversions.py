from refdatatypes.refbool import RefBool


b = RefBool(True)

if bool(b):
    print("It is True")

if b == True:
    print("It is True again")

if b:
    print("It is True again, but with wrong access.")

b.value = False

if not b:
    print("It is False.")

if not bool(b):
    print("It is False too")

if b == False:
    print("Now it is ok again")

if not b.value:
    print("Now it is ok again")
